
<!-- Footer -->
<footer class=" bg-dark text-white ">
  
  <!-- Footer Links -->
  <div class="container text-center text-md-left mt-5">
    
    <!-- Grid row -->
    <div class="row p-3">
      
      <!-- Grid column -->
      <div class="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
        
        <!-- Content -->
        <h6 class="text-uppercase font-weight-bold">Company name</h6>
        <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
        <p>Here you can use rows and columns to organize your footer content. Lorem ipsum dolor sit amet,
          consectetur
          adipisicing elit.</p>
          
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
          
          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Products</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto">
          <p>
            <a href="#!"class="text-white">MDBootstrap</a>
          </p>
          <p>
            <a href="#!"class="text-white">MDWordPress</a>
          </p>
          <p>
            <a href="#!"class="text-white">BrandFlow</a>
          </p>
          <p>
            <a href="#!"class="text-white">Bootstrap Angular</a>
          </p>
          
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class=" col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
          
          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Useful links</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <a href="#!" class="text-white">Your Account</a>
          </p>
          <p>
            <a href="#!" class="text-white">Become an Affiliate</a>
          </p>
          <p>
            <a href="#!" class="text-white">Shipping Rates</a>
          </p>
          <p>
            <a href="#!" class="text-white">Help</a>
          </p>
          
        </div>
        <!-- Grid column -->
        
        <!-- Grid column -->
        <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
          
          <!-- Links -->
          <h6 class="text-uppercase font-weight-bold">Contact</h6>
          <hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto" style="width: 60px;">
          <p>
            <i class="fa fa-home" aria-hidden="true"></i>
            Noida, sec-15</p>
            <p>
              <i class="fa fa-envelope mr-3"></i> info@example.com</p>
              <p>
                <i class="fa fa-phone" aria-hidden="true"></i>
                + 01 234 567 88</p>
                <p>
                  
                </div>
                <!-- Grid column -->
                
              </div>
              <!-- Grid row -->
              
            </div>
            <div class="container">
              <hr>
              
              <!-- Grid row-->
              <div class="row py-4 d-flex justify-content-around">
                
                <!-- Grid column -->
                <div class="col-md-3 col-lg-3 text-center text-md-left mb-4 mb-md-0">
                  <h6 class="mb-0">Get connected with us on social networks!</h6>
                </div>
                <!-- Grid column -->
                
                <!-- Grid column -->
                <div class="col-md-3col-lg-3 text-center text-md-right">
                  
                  <!-- Facebook -->
                  <a class="fb-ic">
                    <i class="fa fa-facebook-f white-text mr-4"> </i>
                  </a>
                  <!-- Twitter -->
                  <a class="tw-ic">
                    <i class="fa fa-twitter white-text mr-4"> </i>
                  </a>
                  
                  
                  <!--Linkedin -->
                  <a class="li-ic">
                    <i class="fa fa-instagram white-text mr-4"> </i>
                    
                  </a>
                  <!--Instagram-->
                  <a class="ins-ic">
                    <i class="fa fa-instagram white-text"> </i>
                  </a>
                  
                </div>
                
                
                <!-- Grid column -->
                
              </div>
              <!-- Grid row-->
              
            </div>
            <!-- Footer Links -->
            
            <!-- Copyright -->
            <hr>
            <div class="footer-copyright text-center py-3">© 2018 Copyright:
              <a href="https://mdbootstrap.com/education/bootstrap/"> pactedge.com.com</a>
            </div>
            <!-- Copyright -->
            
          </footer>
          
          
          <!-- Footer -->
          <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
          <script>
            $(document).ready(function () {
              var itemsMainDiv = ('.MultiCarousel');
              var itemsDiv = ('.MultiCarousel-inner');
              var itemWidth = "";
              
              $('.leftLst, .rightLst').click(function () {
                var condition = $(this).hasClass("leftLst");
                if (condition)
                click(0, this);
                else
                click(1, this)
              });
              
              ResCarouselSize();
              
              
              
              
              $(window).resize(function () {
                ResCarouselSize();
              });
              
              //this function define the size of the items
              function ResCarouselSize() {
                var incno = 0;
                var dataItems = ("data-items");
                var itemClass = ('.item');
                var id = 0;
                var btnParentSb = '';
                var itemsSplit = '';
                var sampwidth = $(itemsMainDiv).width();
                var bodyWidth = $('body').width();
                $(itemsDiv).each(function () {
                  id = id + 1;
                  var itemNumbers = $(this).find(itemClass).length;
                  btnParentSb = $(this).parent().attr(dataItems);
                  itemsSplit = btnParentSb.split(',');
                  $(this).parent().attr("id", "MultiCarousel" + id);
                  
                  
                  if (bodyWidth >= 1200) {
                    incno = itemsSplit[3];
                    itemWidth = sampwidth / incno;
                  }
                  else if (bodyWidth >= 992) {
                    incno = itemsSplit[2];
                    itemWidth = sampwidth / incno;
                  }
                  else if (bodyWidth >= 768) {
                    incno = itemsSplit[1];
                    itemWidth = sampwidth / incno;
                  }
                  else {
                    incno = itemsSplit[0];
                    itemWidth = sampwidth / incno;
                  }
                  $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
                  $(this).find(itemClass).each(function () {
                    $(this).outerWidth(itemWidth);
                  });
                  
                  $(".leftLst").addClass("over");
                  $(".rightLst").removeClass("over");
                  
                });
              }
              
              
              //this function used to move the items
              function ResCarousel(e, el, s) {
                var leftBtn = ('.leftLst');
                var rightBtn = ('.rightLst');
                var translateXval = '';
                var divStyle = $(el + ' ' + itemsDiv).css('transform');
                var values = divStyle.match(/-?[\d\.]+/g);
                var xds = Math.abs(values[4]);
                if (e == 0) {
                  translateXval = parseInt(xds) - parseInt(itemWidth * s);
                  $(el + ' ' + rightBtn).removeClass("over");
                  
                  if (translateXval <= itemWidth / 2) {
                    translateXval = 0;
                    $(el + ' ' + leftBtn).addClass("over");
                  }
                }
                else if (e == 1) {
                  var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
                  translateXval = parseInt(xds) + parseInt(itemWidth * s);
                  $(el + ' ' + leftBtn).removeClass("over");
                  
                  if (translateXval >= itemsCondition - itemWidth / 2) {
                    translateXval = itemsCondition;
                    $(el + ' ' + rightBtn).addClass("over");
                  }
                }
                $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
              }
              
              //It is used to get some elements from btn
              function click(ell, ee) {
                var Parent = "#" + $(ee).parent().attr("id");
                var slide = $(Parent).attr("data-slide");
                ResCarousel(ell, Parent, slide);
              }
              
            });
          </script>
          <script>
            window.addEventListener('load', function() {
              var $ = function($) { return document.querySelector($) },
              img = $('img');
              img.hover = function(onmouseover, onmouseout) {
                this.addEventListener('mouseover', function() { onmouseover(this) });
                this.addEventListener('mouseout', function() { onmouseout(this) })
              };
              img.hover(
              function(el) {
                el.className = el.className + ' transition';
              }, 
              function(el) {
                el.className = el.className.split(' ').filter(function(i){return i != 'transition'}).join(' ');
              });
            }, false);

            // heart toggle code
            
            $(".heart.fa").click(function() {
              $(this).toggleClass("fa-heart fa-heart-o");
            });
            
          </script>
        </body>
        </html>