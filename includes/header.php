<!DOCTYPE html>
<html lang="en">
<head>               
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Navigatio Bar</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
		<div class="container mt-2 mb-2">
			<a class="navbar-brand" href="index.php"> Company</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link " href="index.php">Home <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Dropdown
						</a>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown">
							<a class="dropdown-item" href="#">Action</a>
							<a class="dropdown-item" href="#">Another action</a>
						</li>
					</ul>

					<div class="input-group">
						<input class="form-control col-md-6" type="text" placeholder="Search for brand, clothe and more..." aria-label="Search">
						<div class="input-group-append">
							<span class="input-group-text bg-warning"><i class="fa fa-search " aria-hidden="true"></i></span>
						</div>
					</div>
					<a href="cart.php"><i class="fa fa-shopping-cart text-white fa-2x" aria-hidden="true"></i></a>
				</div>
			</div>
		</nav>
		<!--sub-menu section-->
		<?php include "includes/top-nav.php";?>