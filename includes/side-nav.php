<div class="mt-5">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="card mb-3 p-3">
          <div class="d-flex">
            <i class="fa fa-user-circle-o fa-3x" aria-hidden="true"></i>
            <div class="d-block pl-2">
              <label>Hello,</label>
              <h6>USERNAME INFO</h6>
            </div>
          </div>
        </div>
        <div class="card" >
          <div class="card-header px-4 ">
            <i class="fa fa-list text-primary fa-lg" aria-hidden="true"></i>
            <a href="my-order-list.php">MY ORDERS</a>
          </div>
          <div class="card-header">
            <i class="fa fa-user px-2 text-primary fa-lg" aria-hidden="true"></i>
            <p class="font-weight-bold d-inline text-muted">ACCOUNT SETTINGS</p>
          </div>
          <ul class="list-group list-group-flush ">
            <li class="list-group-item border-0"><a href="#">Profile Information</a></li>
            <li class="list-group-item border-0"><a href="#">Manage Addresses</a></li>
            <li class="list-group-item border-0"><a href="#">PAN Card information</a></li>

          </ul>
          <div class="card-header">
            <i class="fa fa-money px-2 text-primary fa-lg" aria-hidden="true"></i>
            <p class="font-weight-bold d-inline text-muted">PAYMENTS</p>
          </div>
          <ul class="list-group list-group-flush ">
            <li class="list-group-item border-0"><a href="#">Profile Information</a></li>
            <li class="list-group-item border-0"><a href="#">Manage Addresses</a></li>
            <li class="list-group-item border-0"><a href="#">PAN Card information</a></li>

          </ul>
          <div class="card-header">
            <i class="fa fa-envelope px-2 text-primary fa-lg" aria-hidden="true"></i>
            <p class="font-weight-bold d-inline text-muted">MY CHATS</p>
          </div>
          <div class="card-header">
            <i class="fa fa-user px-2 text-primary fa-lg" aria-hidden="true"></i>
            <p class="font-weight-bold d-inline text-muted">MY STUFF</p>
          </div>
          <ul class="list-group list-group-flush ">
            <li class="list-group-item border-0"><a href="#">Profile Information</a></li>
            <li class="list-group-item border-0"><a href="#">Manage Addresses</a></li>
            <li class="list-group-item border-0"><a href="#">PAN Card information</a></li>

          </ul>
          <div class="card-header">
            <i class="fa fa-power-off px-2 text-primary fa-lg" aria-hidden="true"></i>
            <p class="font-weight-bold d-inline text-muted">LOGOUT</p>
          </div>
        </div>

        <div class="card mt-3 p-3">
            <label>Frequently visited</label>
        </div>

      </div> 
