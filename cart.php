<?php include "includes/header.php"; ?>
<section style="height: 100vh;">
    <div class="container ">
        <div class="row mt-5">
            <div class="col-md-8 col-12">
                <div class="card outerDiv2" >

                    <div class="card-header bg-light font-weight-bold">
                        <span><p class="d-inline">MY CART (3)</p> </span> 
                        <span> 
                            <i class="fa fa-map-marker text-center" aria-hidden="true"></i>
                            <span>Deliver to</span>
                        </span>

                        <div class="form-group d-inline float-right">
                            <select class="form-control" id="exampleFormControlSelect1">
                              <option>Address1:xyx</option>
                              <option>2</option>
                              <option>3</option>
                              <option>4</option>
                              <option>5</option>
                          </select>
                      </div>


                  </div>
                  <div class="card-body border-bottom" style="min-height:300px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="images/mobile1.png" class="card-img p-5" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body mt-4">
                                <h5 class="card-title">Product Name</h5>
                                <p class="card-text">Description of product.</p>
                                <p class="card-text text-danger font-weight-bold">status of product.</p>
                                <a href="#" class="btn btn-primary">SAVE FOR LATER</a>
                                <a href="#" class="btn btn-primary">REMOVE</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body border-bottom" style="min-height:300px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="images/mobile1.png" class="card-img p-5" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body mt-4">
                                <h5 class="card-title">Product Name</h5>
                                <p class="card-text">Description of product.</p>
                                <p class="card-text text-danger font-weight-bold">status of product.</p>
                                <a href="#" class="btn btn-primary">SAVE FOR LATER</a>
                                <a href="#" class="btn btn-primary">REMOVE</a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-body border-bottom" style="min-height:300px;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img src="images/mobile1.png" class="card-img p-5" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body mt-4">
                                <h5 class="card-title">Product Name</h5>
                                <p class="card-text">Description of product.</p>
                                <p class="card-text text-danger font-weight-bold">status of product.</p>
                                <a href="#" class="btn btn-primary">SAVE FOR LATER</a>
                                <a href="#" class="btn btn-primary">REMOVE</a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card-footer bg-light font-weight-bold p-4 placeOorderBbutton ">
                <span>
                    <p class="d-inline">MY CART (3)</p>
                </span>
                <span class="float-right">
                    <button type="button" class="btn btn-warning  d-inline my-auto ">PLACE ORDER</button>  

                </span>
            </div>

        </div>
        <div class="col-md-4 col-12">
            <div class="card" style="width: 18rem;">
                <div class="card-header">
                    PRICE DETAILS
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Cras justo odio</li>
                    <li class="list-group-item">Dapibus ac facilisis in</li>
                    <li class="list-group-item">Vestibulum at eros</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- Footer -->
<?php include "includes/footer.php";?>