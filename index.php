<?php include "includes/header.php"; ?>
     <!--single page-carousel-->
     <section class="mt-3">
        <div class="container-fluid">         
          <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active" data-interval="10000">
                <img src="images/festival.jpg" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item" data-interval="2000">
                <img src="images/festival.jpg" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item">
                <img src="images/festival.jpg" class="d-block w-100" alt="...">
              </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
              <span><i class="fa fa-chevron-circle-left fa-2x" aria-hidden="true"></i>
              </span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
              <span><i class="fa fa-chevron-circle-right fa-2x" aria-hidden="true"></i>
              </span>
              
            </a>
          </div>
        </div>
      </section>
      
      <!--single page-carousel-->
    
    <!--carousel--start-->
    <section class="mt-3 mx-auto">
      <div class="container-fluid">
        <div class="row">
          <div class="MultiCarousel" data-items="1,3,4,4" data-slide="1" id="MultiCarousel"  data-interval="1000">
            <div class="MultiCarousel-inner">
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="pad15">
                  <div class="card">
                    <img src="images/jars.jpg" class="card-img-top" alt="...">
                    <div class="card-body   mx-auto">
                      <h5 class="card-title text-center">Jars</h5>
                      <a href="#" class="card-link "><button type="button" class="btn btn-warning">Buy now</button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button class="btn btn-primary leftLst"><</button>
            <button class="btn btn-primary rightLst">></button>
          </div>
        </div>
        
      </div>
      
    </section>
    <!--carousel--end-->
  
    <!--card style-2-->
    <section class="mt-5">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-4 col-12 bg-light  p-2">
            <div class="card  bg-white" style="max-width: 540px;">
              <div class="row no-gutters">
                <div class="col-md-4">
                  <img src="images/mobile1.png" class="card-img p-3" alt="...">
                </div>
                <div class="col-md-8 border-left ">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text bg-dark rounded text-white text-center  mt-5">Price --drop</p>
                    <p class="card-text"><small class="text-muted">discount</small></p>
                    <button type="button" class="btn btn-warning">Buy now</button>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4 col-12  bg-light p-2">
            <div class="card  bg-white" style="max-width: 540px;">
              <div class="row no-gutters">
                <div class="col-md-4">
                  <img src="images/mobile1.png" class="card-img p-3" alt="...">
                </div>
                <div class="col-md-8 border-left ">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text bg-dark rounded text-white text-center  mt-5">Price --drop</p>
                    <p class="card-text"><small class="text-muted">discount</small></p>
                    <button type="button" class="btn btn-warning">Buy now</button>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <div class="col-md-4 col-12 bg-light p-2">
            <div class="card  bg-white" style="max-width: 540px;">
              <div class="row no-gutters">
                <div class="col-md-4">
                  <img src="images/mobile1.png" class="card-img p-3" alt="...">
                </div>
                <div class="col-md-8 border-left ">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text bg-dark rounded text-white text-center  mt-5">Price --drop</p>
                    <p class="card-text"><small class="text-muted">discount</small></p>
                    <button type="button" class="btn btn-warning">Buy now</button>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      
      <!--card style-2 end-->
      <!--Card style 3 with zoom effect-->
      <section class="mt-5" >
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-3 col-12">
              <div class="photo overflow-hidden">
              <img src="images/jars.jpg" alt="This zooms-in really well and smooth">
            </div>
          </div>
          <div class="col-md-3 col-12">
            <div class="photo overflow-hidden">
            <img src="images/jars.jpg" alt="This zooms-in really well and smooth">
          </div>
        </div>
        <div class="col-md-3 col-12">
          <div class="photo overflow-hidden" >
          <img src="images/jars.jpg" alt="This zooms-in really well and smooth">
        </div>
      </div>
      <div class="col-md-3 col-12">
        <div class="photo overflow-hidden" >
        <img src="images/jars.jpg" alt="This zooms-in really well and smooth">
      </div>
    </div>
  </div>
  
</div>
</section>


<!--Card style 3 with zoom effect end-->

<!--card style with heart toggle-->
<section>
  <div class="container-fluid">
    <div class=" row d-flex justify-content-around">
      <div class="col-md-3 col-12">
        <div class="card mt-3" >
          <i class="heart fa fa-heart-o p-2 text-danger"></i>             
          <img src="images/headphone.jpeg" class="card-img-top p-3" alt="...">
          <div class="card-body">
            <h5 class="card-title text-center">sony earphone</h5>
            <p class="card-text text-center">MDR XP 450 ua</p>
            <a href="#" class="btn btn-warning">Best buy</a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-12">
        <div class="card mt-3" >
          <i class="heart fa fa-heart-o p-2 text-danger"></i>             
          
          <img src="images/shoe1.jpeg" class="card-img-top p-3" alt="...">
          <div class="card-body">
            <h5 class="card-title text-center">sony earphone</h5>
            <p class="card-text text-center">MDR XP 450 ua</p>
            <a href="#" class="btn btn-warning">Best buy</a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-12">
        <div class="card mt-3" >
          <i class="heart fa fa-heart-o p-2 text-danger"></i>             
          
          <img src="images/shoes.jpeg" class="card-img-top p-3" alt="...">
          <div class="card-body">
            <h5 class="card-title text-center">sony earphone</h5>
            <p class="card-text text-center">MDR XP 450 ua</p>
            <a href="#" class="btn btn-warning">Best buy</a>
          </div>
        </div>
      </div>
      <div class="col-md-3 col-12">
        <div class="card mt-3" >
          <i class="heart fa fa-heart-o p-2 text-danger"></i>             
          
          <img src="images/headphone.jpeg" class="card-img-top p-3" alt="...">
          <div class="card-body">
            <h5 class="card-title text-center">sony earphone</h5>
            <p class="card-text text-center">MDR XP 450 ua</p>
            <a href="#" class="btn btn-warning">Best buy</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php include "includes/footer.php"; ?>



