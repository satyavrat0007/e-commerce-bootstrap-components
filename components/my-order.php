<div class="container">
	<div class="card mt-2">
		<div class="card-body">
			<div aria-label="breadcrumb">
				<ol class="breadcrumb bg-white">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item"><a href="#">Library</a></li>
					<li class="breadcrumb-item active" aria-current="page">Data</li>
				</ol>
			</div>
			<div class="card mb-2">
				<div class="card-header d-flex justify-content-between">
					<a href="#" class="btn btn-primary">ODIFO11111111111111</a>
					<a href="javascript:void(0)" class="btn btn-outline-primary"> <i class="fa fa-map-marker text-primary" aria-hidden="true"></i> Track</a>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-2">
							<img src="./images/phone.jpeg" style="height: 150px; width: 80px;">
						</div>
						<div class="col-md-4">
							<p>Redmi note 7(4+64 GB, cosmic purple)</p>
							<div>
								<p class="text-muted  mt-5">color:cosmic purple</p>
								<p class="text-muted">seller:xyzz</p>
							</div>
							
						</div>
						<div class="col-md-2">
							<p>₹8999</p>
							<p class="text-danger font-weight-bold">OFFERS:1</p>
						</div>
						<div class="col-md-3">
							<p>Order status</p>
							<p class="text-muted">as per your request your order has been cancelled</p>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<p ><span class="text-muted">Ordered On:</span><span>Tue,23,oct,19</span></p>
				</div>
			</div>
			<div class="card mb-2">
				<div class="card-header d-flex justify-content-between">
					<a href="#" class="btn btn-primary">ODIFO11111111111111</a>
					<a href="javascript:void(0)" class="btn btn-outline-primary"> <i class="fa fa-map-marker text-primary" aria-hidden="true"></i> Track</a>
				</div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-2">
							<img src="./images/phone.jpeg" style="height: 150px; width: 80px;">
						</div>
						<div class="col-md-4">
							<p>Redmi note 7(4+64 GB, cosmic purple)</p>
							<div>
								<p class="text-muted  mt-5">color:cosmic purple</p>
								<p class="text-muted">seller:xyzz</p>
							</div>
							
						</div>
						<div class="col-md-2">
							<p>₹8999</p>
							<p class="text-danger font-weight-bold">OFFERS:1</p>
						</div>
						<div class="col-md-3">
							<p>Order status</p>
							<p class="text-muted">as per your request your order has been cancelled</p>
						</div>
					</div>
				</div>
				<div class="card-footer">
					<p ><span class="text-muted">Ordered On:</span><span>Tue,23,oct,19</span></p>
				</div>
			</div>
		</div>
	</div>
</div>